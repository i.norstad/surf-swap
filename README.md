To run the project locally:
- `python manage.py runserver`
- open in browser at localhost:8000

To access the admin interface:
- localhost:8000/admin
